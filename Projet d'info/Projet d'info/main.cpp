#include <iostream>
#include <string.h>
#include <fstream>
#include <vector>

// definition des using
//fonctions
using std::cin;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
// variables
using std::vector;
using std::string;

// Prototypes
int motdepasse();
void menu();
void scores();
void sauvegarde();
void chargerpartie();
void jouer();

// Ne pas déclarer les classes au dessus car redefintion en dessous... (à mettre dans les .h)
// Classes
class Personnage{};
class Niveau{};
class Objetsduniveau{};

// Heritage
class Snoopy:public Personnage {};
class Balle:public Objetsduniveau {};
class Blocpoussables:public Objetsduniveau {};
class Bloccassables:public Objetsduniveau {};
class Blocpieges:public Objetsduniveau {};


// Sous programmes et main

int main(int argc, const char * argv[]) {
    // insert code here...
    cout << "Hello, World!\n";
    return 0;
}

int motdepasse(Niveau * niv)
{

    string mdpsaisi =" ";

    do
    {
        cout << "Saisir le mdp voulu"<< endl;

        cin >> mdpsaisi;

        if (mdpsaisi== "Niveau 1")
        {
            niv->Niveau1();
            return 1;
        }
        else if (mdpsaisi== "Niveau 2")
        {
            niv->Niveau2();
            return 2;
        }
        else if (mdpsaisi == "Niveau 3")
        {
            niv->Niveau3();
            return 3;
        }
    }while ((mdpsaisi != "Niveau1")&&(mdpsaisi != "Niveau2")&&(mdpsaisi != "Niveau3"));

}

void menu()
{
    int choix=0;

    do
    {
        cout << "Menu" << endl << "1. Jouer" << endl << "2. Charger partie" << endl << "3. Mdp" << endl << "4. Scores"<< endl << "5.Quitter" << endl;
        cout << "Votre choix?"<< endl;
        cin >> choix;

        switch (choix)
        {
            case 1:
                // Jouer
                break;
            case 2:
                // Charger partie
                break;
            case 3:
                // motdepasse
                break;
            case 4:
                //scores
                scores();
                break;
            case 5:
                // quitter
                break;

        }

    }while ((choix < 1)/*ou*/(choix > 5));
}

void scores()
{

    ifstream flux;
    string ligne;
    // on insrit le nom et le score en string => std::vector <std::vector <unsigned long> > tableauscore
    vector < vector < string > > tableauscore;

    int i=0;
    unsigned long taille=0;

    flux.open("Scores.txt");

    if (flux)
    {
        while (getline(flux, ligne))
        {
            tableauscore.push_back();
            tableauscore[i].push_back();

            flux >> tableauscore[i][0];
            flux >> tableauscore[i][1];

            i++;
        }
    }
    else
    {
        cout << " Erreur dans l'ouverture du fichier Score.txt";
    }

    taille = tableauscore.size();

    if (taille != 0)
    {
        for (i=0; i<taille; i++)
        {
            cout << tableauscore[i][0] << " " << tableauscore[i][1] << endl;
        }
    }

}

void sauvegarde() // passer snoopy et niveau en pointeur
{
    string nomfichier;

    cout << "Saisir un fichiyer de sauvegarde" << endl;
    cin >> nomfichier;

    ofstream flux(nomfichier.c_str());

    if (flux)
    {
        /* on écrit dans le fichier sur le format donné dans le sspg chargerpartie() */
    }
}

void chargerpartie() // passer snoopy et niveau en pointeur
{
    // inclure vetcor et fstream

    string monfichier;

    cout << "Saisir le nom de fichier" << endl;
    cin >> monfichier;

    ifstream flux(monfichier.c_str());

    if (flux)
    {
        // Recuperer les infos et passer en mode jouer
        // Format du fichier de sauvegarde
        /*
         niveau
         vie                //Snoopy
         position_x position_y // Snoopy
         score // Snoopy
         nombre d'oiseau // Snoopy
         x1 y1 1/0 x2 y2 1/0 x3 y3 1/0 ..... // Cordonnées x et y de chacun des blocs poussables 1/0 serait le booléen qui permettrait de savoir s'il a déjà été poussé //Niveau
         x1 y1 x2 y2 x3 y3 // Cordonées x et y des blocs cassables, les déjà cassés seraient supprimés de mon tableau de gestion car ils n'apparaissent plus dans la matrice //Niveau
         x1 y1 x2 y2 x3 y3 // Coordonées des blocs piéges, de même ceux déjà cassés n'apparaissent plus // Niveau
         x1 y1 dx1 dx2 x2 y2 dx2 dy2 // coordonnées de(s) balles avec leur(s) vitesse(s) // Niveau
         */
    }
    else
    {
        cout << " Erreur ! Verifiez que le fichier existe bien" << endl;
    }
}

void jouer()
{
    /* Par défaut on est au niveau 1 Niveau1 niv;
     on crée la matrice 10x20 par la ligne niv.Niv1();

     Boucle de jeu
     {
     si la touche p est appuyée => aucun mvt, timmer stopé
     si la touche a est appuyée => casse un bloc
     si la touche s est appuyée => on sauvegarde
     si la touche q est appuyée => on quitte
     si la touche i est appuyée => Snoopy monte
     si la touche j est appuyée => Snoopy va à gauche
     si la touche l est appuyée => Snoopy va à droite
     si la touche k est appuyée => Snoopy descend
     si la touche z et appuyée avec une touche de déplacement => on déplace le bloc dans la direction

     si Snoopy recupere un oiseau => nb_oiseau =+1, si il a les 4 oiseaux, on affiche le message de victoire et on passe au niveau suivant
     si Snoopy est touché => on baisse son niveau de vie, si il est inférieur à 0, on enleve une vie, si plus de vie, on revient au menu

     on augmente le timmer
     on affiche la matrice
     }

     */
}

/// Declaration des classes et heritages

class Personnage
{
protected:

    int m_niv_vie;
    int m_position_x;
    int m_position_y;

public:

    void sedeplacer(int dx, int dy);
    bool est_en_vie();

};

class Snoopy:public Personnage
{
protected:

    int m_nb_vie;
    int m_nb_oiseau;

public:

    bool a_gagne();
    void perd_une_vie();

};

class Niveau
{
protected:

    string m_mdp;
    int m_temps;
    int m_nb_Bloc_cassables;
    int m_nb_Bloc_poussables;
    int m_nb_Bloc_pieges;

    char m_matrice[10][20];
    vector <Blocpoussables> m_tablpoussables();
    vector <Bloccassables> m_tablcassables();
    vector <Blocpieges> m_tablpieges();

public:

    //void modifiertemps();
    //void eliminerbloccassable();
    //void eliminerblocpieges();
    string getMdp();
    void affichermatrice();
};

class Niveau1:public Niveau
{
public:
    Niveau1();
};

class Niveau2:public Niveau
{
protected:

    int m_nb_balles;
    Balle balle1;

public:
    Niveau2();
};

class Niveau3:public Niveau
{
protected:

    int m_nb_balles;
    Balle balle1;
    Balle balle2;

public:
    Niveau3();
};

class Objetsduniveau
{
protected:

    int m_position_x;
    int m_position_y;

};

class Balle:public Objetsduniveau
{
protected:

    int m_vitesse_x;
    int m_vitesse_y;

public:

    void deplacerballe(int m_vitesse_x, int m_vitesse_y);

};

class Blocpoussables:public Objetsduniveau
{
public:

    // bool m_estPousse();
};

class Bloccassables:public Objetsduniveau
{
public:

    // bool m_estCasse();
};

class Blocpieges:public Objetsduniveau
{

public:

    // bool m_aeteTouche();
};


/// Methodes des classes

std::string Niveau::getMdp()
{
    return m_mdp;
}

bool Personnage::est_en_vie()
{
    return m_niv_vie > 0;
}

bool Snoopy::a_gagne()
{
    return (m_nb_vie > 0)&&(m_nb_oiseau == 4)
}

void Personnage::sedeplacer(int depx, int depy)
{
    if (((depx == -1)/*ou*/(depx==1))&&((depy==-1)/*ou*/(depy==1)))
    {
        if (((m_position_x + depx > 0)/*ou*/(m_position_x + depx < 10))&&((m_position_y + depy > 0)/*ou*/(m_position_y + depy <20)))
        {
            m_position_x =+ depx;
            m_position_y =+ depy;
        }
    }
}

void Balle::deplacerballe(int m_vitesse_x, int m_vitesse_y)
{
    if (((m_vitesse_x== -1)/*ou*/(m_vitesse_x==1))&&((m_vitesse_y==-1)/*ou*/(m_vitesse_y==1)))
    {
        if ((m_position_x + m_vitesse_x < 0)/*ou*/(m_position_x + m_vitesse_x > 10))
        {
            m_vitesse_x = - m_vitesse_x;
        }
        if ((m_position_y + m_vitesse_y > 0)/*ou*/(m_position_y + m_vitesse_y <20))
        {
            m_vitesse_y = - m_vitesse_y;
        }
        if (((m_position_x + m_vitesse_x > 0)/*ou*/(m_position_x + m_vitesse_x < 10))&&((m_position_y + m_vitesse_y > 0)/*ou*/(m_position_y + m_vitesse_y <20)))
        {
            m_position_x =+ m_vitesse_x;
            m_position_y =+ m_vitesse_y;
        }

    }
}

void Snoopy::perd_une_vie()
{
    if(!= est_en_vie())
    {
        m_nb_vie =- 1;
    }
}

Niveau1::Niveau1()
{
    int i, j;

    m_mdp = "Niveau 1";
    m_temps = 0;
    m_nb_Bloc_cassables = rand()%5 +1;
    m_nb_Bloc_pieges = rand()% 5 +1;
    m_nb_Bloc_poussables = rand()%5 +1;

    for (i=0; i<10; i++)
    {
        for (j=0; j<20; j++)
        {
            m_matrice[i][j]= ' ';
        }
    }
    m_matrice[0][0] = 'O';
    m_matrice[0][19] = 'O';
    m_matrice[9][0] = 'O';
    m_matrice[9][19] = 'O';

    for (i=0; i<m_nb_Bloc_cassables; i++)
    {
        m_tablcassables.push_back();

        do
        {
            m_tablcassables.push_back();
            m_tablcassables[i].m_position_x = rand()%9 + 1;
            m_tablcassables[i].m_position_y = rand()%19 +1;

            if (m_matrice[m_tablcassables[i].m_position_x][m_tablcassables[i].m_position_y]==' ')
            {
                m_matrice[m_tablcassables[i].m_position_x][m_tablcassables[i].m_position_y]=='C';
            }


        }
    }
    for (i=0; i<m_nb_Bloc_poussables; i++)
    {
        m_tablpoussables.push_back();
        m_tablpoussables[i].m_position_x = rand()%9 + 1;
        m_tablpoussables[i].m_position_y = rand()%19 +1;

        if (m_matrice[m_tablpoussables[i].m_position_x][m_tablpoussables[i].m_position_y]==' ')
        {
            m_matrice[m_tablcassables[i].m_position_x][m_tablcassables[i].m_position_y]=='P';
        }
        // else on refait le tour de boucle sans augmenter i car il y a deux items au même endroit => pas possible

    }
    for (i=0; i<m_nb_Bloc_pieges; i++)
    {
        m_tablpieges.push_back();
        m_tablpieges[i].m_position_x = rand()%9 + 1;
        m_tablpieges[i].m_position_y = rand()%19 +1;

        if (m_matrice[m_tablpieges[i].m_position_x][m_tablpieges[i].m_position_y]==' ')
        {
            m_matrice[m_tablpieges[i].m_position_x][m_tablpieges[i].m_position_y]=='T';
        }
        // else on refait le tour de boucle sans augmenter i car il y a deux items au même endroit => pas possible

    }

}

Niveau2::Niveau2()
{
    int i, j;

    m_mdp = "Niveau 2";
    m_temps = 0;
    m_nb_Bloc_cassables = rand()%10 +1;
    m_nb_Bloc_pieges = rand()% 10 +1;
    m_nb_Bloc_poussables = rand()%10 +1;
    m_nb_balles = 1;

    balle1.m_position_x = rand()%9+1;
    balle1.m_position_y = rand()%19 +1;
    balle1.m_vitesse_x = rand()%6 + 1;
    balle1.m_vitesse_y = rand()%6 +1;

    for (i=0; i<10; i++)
    {
        for (j=0; j<20; j++)
        {
            m_matrice[i][j]= ' ';
        }
    }
    m_matrice[0][0] = 'O';
    m_matrice[0][19] = 'O';
    m_matrice[9][0] = 'O';
    m_matrice[9][19] = 'O';

    m_matrice[balle1.m_position_x][balle1.m_position_y]= 'B';

    for (i=0; i<m_nb_Bloc_cassables; i++)
    {
        m_tablcassables.push_back();
        m_tablcassables[i].m_position_x = rand()%9 + 1;
        m_tablcassables[i].m_position_y = rand()%19 +1;

        if (m_matrice[m_tablcassables[i].m_position_x][m_tablcassables[i].m_position_y]==' ')
        {
            m_matrice[m_tablcassables[i].m_position_x][m_tablcassables[i].m_position_y]=='C';
        }
        // else on refait le tour de boucle sans augmenter i car il y a deux items au même endroit => pas possible

    }
    for (i=0; i<m_nb_Bloc_poussables; i++)
    {
        m_tablpoussables.push_back();
        m_tablpoussables[i].m_position_x = rand()%9 + 1;
        m_tablpoussables[i].m_position_y = rand()%19 +1;

        if (m_matrice[m_tablpoussables[i].m_position_x][m_tablpoussables[i].m_position_y]==' ')
        {
            m_matrice[m_tablcassables[i].m_position_x][m_tablcassables[i].m_position_y]=='P';
        }
        // else on refait le tour de boucle sans augmenter i car il y a deux items au même endroit => pas possible

    }
    for (i=0; i<m_nb_Bloc_pieges; i++)
    {
        m_tablpieges.push_back();
        m_tablpieges[i].m_position_x = rand()%9 + 1;
        m_tablpieges[i].m_position_y = rand()%19 +1;

        if (m_matrice[m_tablpieges[i].m_position_x][m_tablpieges[i].m_position_y]==' ')
        {
            m_matrice[m_tablpieges[i].m_position_x][m_tablpieges[i].m_position_y]=='T';
        }
        // else on refait le tour de boucle sans augmenter i car il y a deux items au même endroit => pas possible

    }

}

Niveau3::Niveau3()
{
    int i, j;

    m_mdp = "Niveau 3";
    m_temps = 0;
    m_nb_Bloc_cassables = rand()%10 +1;
    m_nb_Bloc_pieges = rand()% 10 +1;
    m_nb_Bloc_poussables = rand()%10 +1;

    balle1.m_position_x = rand()%5+1; // la balle 1 apparaitra sur la partie haute de la matrice
    balle1.m_position_y = rand()%19 +1;
    balle1.m_vitesse_x = rand()%6 + 1;
    balle1.m_vitesse_y = rand()%6 +1;
    balle2.m_position_x = rand()%5 + 6; // la balle 2 apparaitra sur la partie basse de la matrice
    balle2.m_position_y = rand()%19 +1;
    balle2.m_vitesse_x = rand()%6 + 1;
    balle2.m_vitesse_y = rand()%6 +1;


    for (i=0; i<10; i++)
    {
        for (j=0; j<20; j++)
        {
            m_matrice[i][j]= ' ';
        }
    }
    m_matrice[0][0] = 'O';
    m_matrice[0][19] = 'O';
    m_matrice[9][0] = 'O';
    m_matrice[9][19] = 'O';

    m_matrice[balle1.m_position_x][balle1.m_position_y]= 'B';
    m_matrice[balle2.m_position_x][balle2.m_position_y]= 'B';

    for (i=0; i<m_nb_Bloc_cassables; i++)
    {
        m_tablcassables.push_back();
        m_tablcassables[i].m_position_x = rand()%9 + 1;
        m_tablcassables[i].m_position_y = rand()%19 +1;

        if (m_matrice[m_tablcassables[i].m_position_x][m_tablcassables[i].m_position_y]==' ')
        {
            m_matrice[m_tablcassables[i].m_position_x][m_tablcassables[i].m_position_y]=='C';
        }
        // else on refait le tour de boucle sans augmenter i car il y a deux items au même endroit => pas possible

    }
    for (i=0; i<m_nb_Bloc_poussables; i++)
    {
        m_tablpoussables.push_back();
        m_tablpoussables[i].m_position_x = rand()%9 + 1;
        m_tablpoussables[i].m_position_y = rand()%19 +1;

        if (m_matrice[m_tablpoussables[i].m_position_x][m_tablpoussables[i].m_position_y]==' ')
        {
            m_matrice[m_tablcassables[i].m_position_x][m_tablcassables[i].m_position_y]=='P';
        }
        // else on refait le tour de boucle sans augmenter i car il y a deux items au même endroit => pas possible

    }
    for (i=0; i<m_nb_Bloc_pieges; i++)
    {
        m_tablpieges.push_back();
        m_tablpieges[i].m_position_x = rand()%9 + 1;
        m_tablpieges[i].m_position_y = rand()%19 +1;

        if (m_matrice[m_tablpieges[i].m_position_x][m_tablpieges[i].m_position_y]==' ')
        {
            m_matrice[m_tablpieges[i].m_position_x][m_tablpieges[i].m_position_y]=='T';
        }
        // else on refait le tour de boucle sans augmenter i car il y a deux items au même endroit => pas possible

    }

}

void Niveau::public affichermatrice()
{
    int i, j;

    for (i=0; i<10; i++)
    {
        for (j=0; j<20; j++)
        {
            cout << m_matrice[i][j];
        }
        cout << endl;
    }
}
