#include <iostream>
#include <stdlib.h>
#include <windows.h>
#include "console.h"
#include "MenuSrc.h"


using std::cout; using std::endl; using std::string; using std::cin;

void menu()
{
    bool quit = false;
    Console* pConsole = NULL;
    // Alloue la m�moire du pointeur
    pConsole = Console::getInstance();
    system("cls"); //retire tout affichage de la console
    Snoop(pConsole); //affichage du logo
    pConsole->setColor(COLOR_DEFAULT);pConsole->gotoLigCol(59,130);cout<<"Appuyez sur ESC pour quitter."; //option escape "d'urgence"
    Doggy(pConsole, 30, 100); // affichage du chien
    pConsole->setColor(COLOR_DEFAULT);pConsole->gotoLigCol(11,20);
    cout<<"1. Jouer (un seul joueur)";pConsole->gotoLigCol(12,20); //key =49
    cout<<"2. Charger une partie";pConsole->gotoLigCol(13,20);//key =50
    cout<<"3. Mot de passe";pConsole->gotoLigCol(14,20);//key =51
    cout<<"4. Scores";pConsole->gotoLigCol(15,20);//key =52
    cout<<"5. Quitter";pConsole->gotoLigCol(16,20);//key =53
    // Boucle �v�nementielle
    while (!quit)
    {
        // Si on a appuy� sur une touche du clavier
        if (pConsole->isKeyboardPressed())
        {
            // R�cup�re le code ASCII de la touche
            int key=pConsole->getInputKey();

            switch(key){
        case 49: //jouer
            LevelSelection(pConsole);
            cout<< "nothing happens for now."<<endl;
            break;
        case 50: //charger une partie
            ChargementPartie(pConsole);
            cout<< "nothing happens for now."<<endl;
            break;
        case 51: //mot de passe
            quit=true;
            CheatCodes(pConsole);
            break;
        case 52: //scores
            break;
        case 53: //touche 5 = quitter
            quitMessage(pConsole);
            quit=true;
            break;
        case 27: //touche escape = quitter
            quit=true;
            system("exit");
            break;
        case 248: //easter egg TODO
            cout << "Easter egg";
            break;
        default : //default for checking key number
            cout << key;
            break;

            }
        }
    }

    // Lib�re la m�moire du pointeur !
    Console::deleteInstance();

}

void CheatCodes(Console * pConsole) //fonctions pour les mdp
{
    string mdp; string ActualPass = "hello"; //we will define the passwords of each level in here instead of declaring them as attributes
    pConsole->gotoLigCol(19,20);pConsole->setColor(COLOR_BLUE);
    cout << "Entrez votre mot de passe ou 0 pour revenir au menu : ";
    pConsole->setColor(COLOR_YELLOW);cin >> mdp;
    while(mdp!=ActualPass && mdp!="0"){ //tant que le mot de passe ne correspond pas � un des mdp des niveaux, on dis non.
                                        // l'utilisateur doit entrer "0" pour retourner au menu
        pConsole->gotoLigCol(19,73);pConsole->setColor(COLOR_RED);cout<<"\t\t\t\t\t Ce mot de passe ne correspond a aucun niveau.";
        pConsole->gotoLigCol(19,74);pConsole->setColor(COLOR_YELLOW);
        cin >> mdp;
    }
    if(mdp=="0"){menu();} //option retour au menu
    if(mdp==ActualPass){pConsole->gotoLigCol(50,50);Sleep(2000);cout<<"Nice Job you know the password";}
}

void quitMessage(Console * pConsole){
pConsole->setColor(COLOR_RED);pConsole->gotoLigCol(40,20);cout<<" _____ ";pConsole->gotoLigCol(41,20);cout<<"| __  |";pConsole->gotoLigCol(42,20);cout<<"| __ -|";pConsole->gotoLigCol(43,20);cout<<"|_____|";Sleep(250);
pConsole->setColor(COLOR_GREEN);pConsole->gotoLigCol(40,27);cout<<" __ __ ";pConsole->gotoLigCol(41,27);cout<<"|  |  |";pConsole->gotoLigCol(42,27);cout<<"|_   _|";pConsole->gotoLigCol(43,27);cout<<"  |_|  ";Sleep(250);
pConsole->setColor(COLOR_YELLOW);pConsole->gotoLigCol(40,34);cout<<" _____ ";pConsole->gotoLigCol(41,34);cout<<"|   __|";pConsole->gotoLigCol(42,34);cout<<"|   __|";pConsole->gotoLigCol(43,34);cout<<"|_____|";Sleep(250);
cout << "\n\n";
}

void Snoop(Console * pConsole){ //affichage du logo
    pConsole->gotoLigCol(5, 55);
    pConsole->setColor(COLOR_GREEN);
    cout<<" _____ _____ _____ _____ _____ __ __ ";pConsole->gotoLigCol(6,55);
    cout<<"|   __|   | |     |     |  _  |  |  |";pConsole->gotoLigCol(7,55);
    cout<<"|__   | | | |  |  |  |  |   __|_   _|";pConsole->gotoLigCol(8,55);
    cout<<"|_____|_|___|_____|_____|__|    |_|  ";
}

void Doggy(Console * pConsole, int line, int column){ //affichage du chien (selon des coordonn�es pour possible r�utilisation)
pConsole->setColor(COLOR_RED);pConsole->gotoLigCol(line,column);
    cout<<"  ,-~~-.___.";pConsole->gotoLigCol(line+1,column);
    cout<<" / |  '     \\";pConsole->gotoLigCol(line+2,column);
    cout<<"(  )         0";pConsole->gotoLigCol(line+3,column);
    cout<<" \\_/-, ,----'";pConsole->gotoLigCol(line+4,column);
    cout<<"    ====           //";pConsole->gotoLigCol(line+5,column);
    cout<<"   /  \\-'~;    /~~~(O)";pConsole->gotoLigCol(line+6,column);
    cout<<"  /  __/~|   /       |";pConsole->gotoLigCol(line+7,column);
    cout<<"=(  _____| (_________|";

}
void LevelSelection(Console * pConsole){ // this is where we display the level selection screen
    // levels will be declared here (for now)

}

void ChargementPartie(Console * pConsole){ // On propose au joueur plusieurs Noms de joueurs sauvegard�s aupparavant.
    //si une partie �tait en cours on lui propose de reprendre cette partie
    //sinon on lui propose de continuer la "campagne" --> progression des niveaux

}

void AffichageScores(Console * pConsole){ // affichage des scores
    //les scores sont sauv�s dans un fichier diff�rent de celui du joueur
    //fichier = $$Joueur : Niveau N : X points ---
    //lecture et affichage du fichier

}
