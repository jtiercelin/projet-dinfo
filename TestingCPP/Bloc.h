#ifndef BLOC_H_INCLUDED
#define BLOC_H_INCLUDED

class Bloc{
public:
    void setLetter(char lettre); // pas s�r de si on a vraiment besoin de donner une lettre au bloc, ou si on peut tout simplement se charger de cet �l�ment dans l'affichage du niveau.
    char getLetter(void);
    void setBloc(Bloc unBloc);
    Bloc getBloc(void);

private:
    char m_letter;

}; //on d�clarerai ensuite tous les blocs en tant qu'h�ritages avec leurs diff�rents attributs


#endif // BLOC_H_INCLUDED
