#ifndef MENUSRC_H_INCLUDED
#define MENUSRC_H_INCLUDED

void menu(void);
void CheatCodes(Console * pConsole);
void quitMessage(Console * pConsole);
void LevelSelection(Console * pConsole);
void ChargementPartie(Console * pConsole);
void Snoop(Console * pConsole);
void Doggy(Console * pConsole, int line, int column);

#endif // MENUSRC_H_INCLUDED
