#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

class Player{
public:
    void setName(string Name);
    string getName(void);
    void setLives(int lives);
    int getLives(void);
    void setSave(FILE * Save);
    FILE * getSave(void);
    void PlayLevel(Niveau UnNiveau); //Pour faciliter la sauvegarde c'est le joueur qui lance le niveau et pas le niveau qui se lance tout seul

private:
    string m_PlayerName;
    int m_lives;
    FILE * m_SaveFile;
};

#endif // PLAYER_H_INCLUDED
