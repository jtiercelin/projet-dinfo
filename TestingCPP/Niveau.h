#ifndef NIVEAU_H_INCLUDED
#define NIVEAU_H_INCLUDED

class Niveau{
public :
    void setTime(float time); //getters et settters // probablement besoin d'une variable time sp�cifique au niveau pour l'option de PAUSE
    float getTime(void);
    void setCheatCode(string mdp);
    string getCheatCode(void);
    void setWon(bool YesNo); // je pense qu'on a besoin d'un bool�en won et un bool�en lost, pour moi impossible de regrouper les 2 en un seul lors des boucles de test
    bool getWon(void);
    void setLost(bool YesNo);
    bool getLost(void);
    void afficherNiveau(void); //simple affichage du niveau sous forme de matrice de char

private :
    string m_MDP;
    Bloc m_leNiveau[8][9];
    float m_timeLeft;
    bool m_won; bool m_lost;
};

#endif // NIVEAU_H_INCLUDED
